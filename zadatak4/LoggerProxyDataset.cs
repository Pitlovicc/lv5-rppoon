﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak4
{
    class LoggerProxyDataset : IDataset
    {
        Dataset dataset;
        ConsoleLogger consoleLogger;
        

        public LoggerProxyDataset()
        {
            consoleLogger = ConsoleLogger.GetInstance();
        }

        public ReadOnlyCollection<List<string>> GetData()
        {
            consoleLogger.Log("Vrijeme pristupa");
            if (dataset == null)
            {
                dataset = new Dataset(@"E:\2. godina ferit\4. semestar\RPOON\LV5\CSV.txt");
            }
            return dataset.GetData();
        }
    }
}
