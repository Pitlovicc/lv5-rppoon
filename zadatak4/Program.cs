﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            LoggerProxyDataset loggerProxyDadaset = new LoggerProxyDataset();
            DataConsolePrinter dataConsolePrinter = new DataConsolePrinter();
            dataConsolePrinter.Print(loggerProxyDadaset);
        }
    }
}
