﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak12
{
    class ShippingService
    {
        private double pricePerKg;
        public ShippingService(double pricePerKg)
        {
            this.pricePerKg = pricePerKg;
        }
         
        public double calculatePrice(IShipable shipable) 
        {
            double price;
            price = shipable.Weight * this.pricePerKg;
            return price;
            
        }
        
    }
}
