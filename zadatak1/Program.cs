﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak12
{
    class Program
    {
        static void Main(string[] args)
        {
            Box bicycle = new Box("cycling stuff");
            Product glasses = new Product("Glasses", 15, 0.8);
            Product helmet = new Product("Helmet", 40, 2.5 );
            Product bike = new Product("Trek bicycle", 1000, 15);
            ShippingService shippingService = new ShippingService(10);
            bicycle.Add(glasses);
            bicycle.Add(helmet);
            bicycle.Add(bike);
            Console.WriteLine("Price of the shipping for the cycling box:");
            Console.WriteLine(shippingService.calculatePrice(bicycle));

            Box clothes = new Box("clothing stuff");
            Product hat = new Product("Hat", 20, 1.5);
            Product shirt = new Product("Shirt", 15, 1);

            Console.WriteLine("Price of the shipping for the clothing box:");
            clothes.Add(hat);
            clothes.Add(shirt);
            Console.WriteLine(shippingService.calculatePrice(clothes));

            clothes.Add(bicycle);
            Console.WriteLine("Price of the shipping for the cycling stuff box in the clothing box:");
            Console.WriteLine(shippingService.calculatePrice(clothes));

        }
    }
}
