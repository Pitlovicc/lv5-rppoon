﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak3
{
    class DataConsolePrinter
    {
        public DataConsolePrinter() { }
        public void Print(IDataset data)
        {
            IReadOnlyCollection<List<string>> Data = data.GetData();
            if (Data == null)
            {
                Console.WriteLine("fail");
                return;
            }
            foreach (List<string> lists in Data)
            {
                foreach (string list in lists)
                {
                    Console.Write(list + ' ');
                }
                Console.WriteLine();
            }
        }
    }
}
