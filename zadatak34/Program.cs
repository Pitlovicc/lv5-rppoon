﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak3
{
    class Program
    {
        static void Main(string[] args)
        {
            
            ///virtualni sa lijenom inicijalizacijom
            VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset(@"E:\2. godina ferit\4. semestar\RPOON\LV5\CSV.txt");
            DataConsolePrinter consolePrinter = new DataConsolePrinter();
            consolePrinter.Print(virtualProxyDataset);
            Console.WriteLine(" ");
            ///zaštitni
            User user1 = User.GenerateUser("Korisnik 1"); ///id 1 
            User user2 = User.GenerateUser("Korisnik 2"); /// id 2
            User user3 = User.GenerateUser("Korisnik 3");/// id 3
            User user4 = User.GenerateUser("Korisnik 4");/// id 3
            ProtectionProxyDataset protection1 = new ProtectionProxyDataset(user1);
            ProtectionProxyDataset protection2 = new ProtectionProxyDataset(user2);
            ProtectionProxyDataset protection3 = new ProtectionProxyDataset(user3);
            ProtectionProxyDataset protection4 = new ProtectionProxyDataset(user4);

            Console.WriteLine("Korisnik s id-om '1' ");
            consolePrinter.Print(protection1);
            Console.WriteLine("Korisnik s id-om '12' ");
            consolePrinter.Print(protection2);
            Console.WriteLine("Korisnik s id-om '3' ");
            consolePrinter.Print(protection3);
            Console.WriteLine("Korisnik s id-om '4' ");
            consolePrinter.Print(protection4);
        }
       
    }
} 
