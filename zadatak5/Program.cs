﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme lightTheme = new LightTheme();
            lightTheme.GetHeader(2);
            lightTheme.GetFooter(1);

            ReminderNote reminderNote = new ReminderNote("Konkretka zabiljeska", lightTheme);
            reminderNote.Show();

            
            YellowRedTheme yellowRedTheme = new YellowRedTheme();
            yellowRedTheme.GetHeader(4);
            yellowRedTheme.GetFooter(3);

            ReminderNote reminderNote2 = new ReminderNote("Žuto-crvena tema", yellowRedTheme);
            reminderNote2.Show();

        }
    }
}
