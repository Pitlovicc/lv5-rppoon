﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak7
{
    class ListOfNames : Note
    {
        private List<string> names;

        public ListOfNames(string message, ITheme theme) : base(message, theme) {
            this.names = new List<string>();
        }

        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("REMINDER: ");
            string framedMessage = this.GetFramedMessage();
            Console.WriteLine(framedMessage);
            foreach(string name in names)
            {
                Console.WriteLine(name);
            }
            Console.ResetColor();

        }
        public void Add(string name)
        {
            this.names.Add(name);
        }
        public void Remove(string name)
        {
            this.names.Remove(name);
        }
    }
}
