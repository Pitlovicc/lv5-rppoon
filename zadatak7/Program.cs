﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme lightTheme = new LightTheme();
            lightTheme.GetHeader(2);
            lightTheme.GetFooter(1);

            YellowRedTheme yellowRedTheme = new YellowRedTheme();
            yellowRedTheme.GetHeader(4);
            yellowRedTheme.GetFooter(3);

            ListOfNames listOfNames1 = new ListOfNames("Viber grupa", lightTheme);
            ListOfNames listOfNames2 = new ListOfNames("WhatsApp grupa", yellowRedTheme);
            listOfNames1.Add("Marija");
            listOfNames1.Add("Karlo");
            listOfNames1.Add("Stjepan");

            listOfNames2.Add("Antun");
            listOfNames2.Add("Fran");
            listOfNames2.Add("Bruno");
            //listOfNames1.Show();
            //listOfNames2.Show();

            //7. zadatak 1. dio

            //Notebook notebook = new Notebook();
            //notebook.AddNote(listOfNames1);
            //notebook.AddNote(listOfNames2);
            ////notebook.Display();

            //7.zadatak 2.dio
            //notebook.ChangeTheme(lightTheme);
            //notebook.Display();

            //7.zadatak 3. dio - kad dodam žuto-crvenu temu trebala bi se promijenit odma u light zelenu
            Notebook notebook2 = new Notebook();
            notebook2.AddNote(listOfNames2,lightTheme);
            notebook2.Display();

        }
    }
}
