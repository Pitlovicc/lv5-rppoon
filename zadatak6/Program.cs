﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme lightTheme = new LightTheme();
            lightTheme.GetHeader(2);
            lightTheme.GetFooter(1);

            YellowRedTheme yellowRedTheme = new YellowRedTheme();
            yellowRedTheme.GetHeader(4);
            yellowRedTheme.GetFooter(3);

            ListOfNames listOfNames1 = new ListOfNames("Viber grupa", lightTheme);
            ListOfNames listOfNames2 = new ListOfNames("WhatsApp grupa", yellowRedTheme);
            listOfNames1.Add("Marija");
            listOfNames1.Add("Karlo");
            listOfNames1.Add("Stjepan");

            listOfNames2.Add("Antun");
            listOfNames2.Add("Fran");
            listOfNames2.Add("Bruno");
            listOfNames1.Show();
            listOfNames2.Show();

        }
    }
}
